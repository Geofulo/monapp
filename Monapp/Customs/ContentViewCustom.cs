﻿using System;
using Xamarin.Forms;

namespace Monapp
{
	public class ContentViewCustom : ContentView
	{
		public static readonly BindableProperty IsDarkBlurProperty =
			BindableProperty.Create<ContentViewCustom, Boolean>(
				p => p.IsDarkBlur, false);

		public Boolean IsDarkBlur {
			get { return (Boolean)GetValue(IsDarkBlurProperty); }
			set { SetValue(IsDarkBlurProperty, value); }
		}

		public static readonly BindableProperty IsLightBlurProperty =
			BindableProperty.Create<ContentViewCustom, Boolean>(
				p => p.IsLightBlur, false);

		public Boolean IsLightBlur {
			get { return (Boolean)GetValue(IsLightBlurProperty); }
			set { SetValue(IsLightBlurProperty, value); }
		}

		public static readonly BindableProperty RadiusProperty =
			BindableProperty.Create<ContentViewCustom, int>(
				p => p.Radius, 0);

		public int Radius {
			get { return (int)GetValue(RadiusProperty); }
			set { SetValue(RadiusProperty, value); }
		}

		public static readonly BindableProperty IsGradientProperty =
			BindableProperty.Create<ContentViewCustom, Boolean>(
				p => p.IsGradient, false);

		public Boolean IsGradient {
			get { return (Boolean)GetValue(IsGradientProperty); }
			set { SetValue(IsGradientProperty, value); }
		}

		public static readonly BindableProperty GradientStartColorProperty =
			BindableProperty.Create<ContentViewCustom, Color>(
				p => p.GradientStartColor, Color.Transparent);

		public Color GradientStartColor {
			get { return (Color)GetValue(GradientStartColorProperty); }
			set { SetValue(GradientStartColorProperty, value); }
		}

		public static readonly BindableProperty GradientEndColorProperty =
			BindableProperty.Create<ContentViewCustom, Color>(
				p => p.GradientEndColor, Color.Transparent);

		public Color GradientEndColor {
			get { return (Color)GetValue(GradientEndColorProperty); }
			set { SetValue(GradientEndColorProperty, value); }
		}

		public static readonly BindableProperty IsShadowProperty =
			BindableProperty.Create<ContentViewCustom, Boolean>(
				p => p.IsShadow, false);

		public Boolean IsShadow {
			get { return (Boolean)GetValue(IsShadowProperty); }
			set { SetValue(IsShadowProperty, value); }
		}
	}
}

