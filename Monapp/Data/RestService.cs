﻿using System;
using Telerik.Everlive.Sdk.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Monapp
{
	public class RestService
	{
		EverliveApp EveApp;

		public RestService ()
		{
			EverliveAppSettings appSettings = new EverliveAppSettings {
				AppId = "uyk2yik9z5qpsm4v",
				TestMode = true,
				//UseHttps = true
			};
			EveApp = new EverliveApp (appSettings);
		}

		public List<ModeloModel> GetModelos () 
		{
			var tcs = new TaskCompletionSource<List<ModeloModel>> ();

			var res = new List<ModeloModel> ();

			new Task ( async () => {
				try {
					var result = await EveApp.WorkWith().Data<ModeloModel>().GetAll().ExecuteAsync();

					foreach (var r in result)
					{
						res.Add (r);
					}

					System.Diagnostics.Debug.WriteLine (res + " modelos");

					tcs.SetResult (res);
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine (e.Message);
					tcs.SetResult (null);
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public List<ModeloModel> GetModelosAvailables () 
		{
			var tcs = new TaskCompletionSource<List<ModeloModel>> ();

			var res = new List<ModeloModel> ();

			new Task ( async () => {
				try {
					var result = await EveApp.WorkWith().Data<ModeloModel>().GetAll().ExecuteAsync();

					foreach (var r in result)
					{
						if (r.IsAvailable)
							res.Add (r);
					}

					System.Diagnostics.Debug.WriteLine (res + " modelos disponibles");

					tcs.SetResult (res);
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine (e.Message);
					tcs.SetResult (null);
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public string GetDownloadLink(Guid fileId)
		{
			return EveApp.WorkWith().Files().GetFileDownloadUrl(fileId);
		}
	}
}

