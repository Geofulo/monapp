﻿using System;
using Xamarin.Forms;

namespace Monapp
{
	public static class ColorResources
	{
		public static Color NavBar = Color.FromHex ("282b36");
		public static Color NavBarText = Color.FromHex ("e5d4c3");
		//public static Color Background = Color.White;
		public static Color Background = Color.FromHex ("2c303b");
		public static Color Primary = Color.Black;

		public static Color BackgroundItem = Color.FromHex ("fb668e");
		public static Color BackgroundItemSecondary = Color.FromHex ("f23769");
		public static Color TextColorItem = Color.White;

		//public static Color BackgroundMenu = Color.FromHex ("73e7cb");
		//public static Color BackgroundMenuSecondary = Color.FromHex ("48b2af");
		public static Color BackgroundMenu = Color.FromHex ("363942");
		public static Color BackgroundMenuSecondary = Color.FromHex ("363942");
		public static Color TextColorMenu = Color.FromHex ("e5d4c3");

		public static Color BackgroundContentDetail = Color.White;

		//public static Color Second = Color.FromHex ("");
	}
}

