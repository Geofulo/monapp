﻿using System;
using Telerik.Everlive.Sdk.Core.Model.Base;
using Telerik.Everlive.Sdk.Core.Serialization;

namespace Monapp
{
	[ServerType("Modelo")]
	public class ModeloModel : DataItem
	{
		public string Nombre { get; set; }

		public string Descripcion { get; set; }

		public bool IsAvailable { get; set; }

		public int Price { get; set; }

		public Guid[] Imagenes { get; set; }
	}
}

