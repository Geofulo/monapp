﻿using System;
using Telerik.Everlive.Sdk.Core.Serialization;

namespace Monapp
{
	[ServerType("PedidoModelo")]
	public class PedidoModelo
	{
		public ModeloModel Modelo { get; set; }

		public int Cantidad { get; set; }

		public string Colores { get; set; }
	}
}

