﻿using System;

using Xamarin.Forms;

namespace Monapp
{	
	public class App : Application
	{
		static public int ScreenWidth;
		static public int ScreenHeight;
		public static RestService WSService;

		public App ()
		{
			WSService = new RestService ();

			MainPage = new Monapp.MainPage();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

