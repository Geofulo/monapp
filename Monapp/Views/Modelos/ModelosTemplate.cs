﻿using System;
using Xamarin.Forms;

namespace Monapp
{
	public class ModelosTemplate : ContentViewCustom
	{
		ModeloModel Modelo;

		TapGestureRecognizer TapGesture = new TapGestureRecognizer();

		RelativeLayout Relative;

		public ModelosTemplate (ModeloModel Modelo)
		{
			this.Modelo = Modelo;

			createLayouts ();
			setLayouts ();
			setListeners ();

			GestureRecognizers.Add (TapGesture);
			WidthRequest = (App.ScreenWidth / 2) - 15;
			HeightRequest = 220;

			IsGradient = true;
			GradientStartColor = ColorResources.BackgroundItem;
			GradientEndColor = ColorResources.BackgroundItemSecondary;

			Radius = 5;
			IsClippedToBounds = true;

			IsShadow = true;

			Content = Relative;
		}

		void createLayouts ()
		{
			Relative = new RelativeLayout {
				BackgroundColor = Color.Transparent,
			};
		}

		void setLayouts ()
		{
			Relative.Children.Add (
				new Image {
					Source = ImageSource.FromUri (new Uri (App.WSService.GetDownloadLink (Modelo.Imagenes [0]))),
					Aspect = Aspect.AspectFill,
					WidthRequest = (App.ScreenWidth / 2) - 15,
					HeightRequest = 220,
					HorizontalOptions = LayoutOptions.FillAndExpand,
				},
				Constraint.Constant (0),
				Constraint.Constant (0)
			);
			Relative.Children.Add (
				new ContentViewCustom {
					IsGradient = true,
					GradientStartColor = Color.Transparent,
					GradientEndColor = Color.Black,
					WidthRequest = (App.ScreenWidth / 2) - 15,
					HeightRequest = 220,
				},
				Constraint.Constant (0),
				Constraint.Constant (0)
			);
			Relative.Children.Add (
				new StackLayout {
					Padding = new Thickness (5, 10),
					BackgroundColor = Color.Transparent,
					WidthRequest = (App.ScreenWidth / 2) - 15,
					HeightRequest = 200,
					//VerticalOptions = LayoutOptions.EndAndExpand,
					Children = {
						new Label {
							Text = Modelo.Nombre,
							FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
							FontAttributes = FontAttributes.Bold,
							TextColor = ColorResources.NavBarText,
							HorizontalTextAlignment = TextAlignment.Center,
							HorizontalOptions = LayoutOptions.CenterAndExpand,
							VerticalOptions = LayoutOptions.EndAndExpand,
						},
						new Label {
							Text = Modelo.Descripcion,
							FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
							TextColor = ColorResources.TextColorItem,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							VerticalOptions = LayoutOptions.End,
						},
						new Label {
							Text = "$" + Modelo.Price,
							FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
							TextColor = ColorResources.TextColorItem,
							HorizontalOptions = LayoutOptions.EndAndExpand,
							VerticalOptions = LayoutOptions.End,
						}
					}
				},
				Constraint.Constant (0),
				Constraint.Constant (0)
			);
		}
			
		void setListeners ()
		{
			TapGesture.Tapped += (sender, e) => Navigation.PushAsync (new DetailModeloPage (Modelo));
		}
	}
}

