﻿using System;
using Xamarin.Forms;

namespace Monapp
{
	public class ModelosPage : TabbedPage
	{
		public ModelosPage ()
		{
			Title = "Modelos";

			BackgroundColor = ColorResources.NavBar;

			Children.Add (new DisponiblesModelosPage());
			Children.Add (new TodosModelosPage());
		}
	}
}

