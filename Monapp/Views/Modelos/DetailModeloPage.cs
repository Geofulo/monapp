﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Monapp
{
	public class DetailModeloPage : ContentPage
	{
		ModeloModel Modelo;

		List<Entry> CantidadEntries = new List<Entry> ();
		List<Entry> ColoresEntries = new List<Entry> ();

		List<Button> DeleteRowPedidosButtons = new List<Button> ();
		Button AddRowPedidosButton;

		Button AddCarritoButton;

		StackLayout PedidoStack;

		StackLayout Stack;

		public DetailModeloPage (ModeloModel Modelo)
		{
			this.Modelo = Modelo;

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			BackgroundColor = ColorResources.Background;
			Padding = new Thickness (10);

		//	Content = Stack;
		}

		void createElements ()
		{
			CantidadEntries.Add(getCantidadEntry ());

			ColoresEntries.Add(getColoresEntry ());

			DeleteRowPedidosButtons.Add(getDeleteButton ());

			AddCarritoButton = new Button {
				Text = "Agregar al carrito",
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Button)),
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			AddRowPedidosButton = new Button {
				Text = "Agregar",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Button)),
				HorizontalOptions = LayoutOptions.StartAndExpand,
			};
		}

		void createLayouts ()
		{			
			PedidoStack = new StackLayout {
				Padding = new Thickness (10, 5),
				Spacing = 10,
			};

			Stack = new StackLayout {
				Spacing = 15,
			};
		}

		void setLayouts ()
		{
			var stackInfo = new StackLayout {
				Spacing = 15,
			};


			stackInfo.Children.Add (new StackLayout {
				Padding = new Thickness (10, 0),
				Children = {
					new Label {
						Text = Modelo.Nombre,
						FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
						TextColor = ColorResources.NavBar,
						HorizontalOptions = LayoutOptions.FillAndExpand,
					},
					new Label {
						Text = Modelo.Descripcion,
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						TextColor = ColorResources.NavBar,
						HorizontalOptions = LayoutOptions.FillAndExpand,
					}
				}
			});

			if (Modelo.IsAvailable) {
				
				PedidoStack.Children.Add (new StackLayout {
					Spacing = 10,
					Orientation = StackOrientation.Horizontal,
					Children = {
						new Label {
							Text = "Cantidad",
							FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
							WidthRequest = 90,
						},
						new Label {
							Text = "Colores",
							FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				});
				PedidoStack.Children.Add (new StackLayout {
					Spacing = 10,
					Orientation = StackOrientation.Horizontal,
					Children = {								
						CantidadEntries[0],
						ColoresEntries[0],
						DeleteRowPedidosButtons[0]
					}
				});
				PedidoStack.Children.Add (AddRowPedidosButton);
						
				stackInfo.Children.Add (PedidoStack);
							
			}

			Stack.Children.Add (new ContentViewCustom {
				BackgroundColor = ColorResources.BackgroundContentDetail,
				Radius = 10,
				Padding = new Thickness (10),
				WidthRequest = App.ScreenWidth - 20,
				//HeightRequest = App.ScreenHeight - 20,
				Content = new ScrollView {
					Content = Stack,
				}
			});

			/*
			if (Modelo.IsAvailable) 
				Stack.Children.Add (AddCarritoButton);
				*/
		}

		void setListeners ()
		{
			AddRowPedidosButton.Clicked += (sender, e) => {
				var cantidadEntry = getCantidadEntry ();
				var coloresEntry = getColoresEntry ();
				var deleteButton = getDeleteButton ();

				CantidadEntries.Add (cantidadEntry);
				ColoresEntries.Add (coloresEntry);
				DeleteRowPedidosButtons.Add (deleteButton);

				PedidoStack.Children.Insert (
					PedidoStack.Children.Count - 2, 
					new StackLayout {
						Spacing = 10,
						Orientation = StackOrientation.Horizontal,
						Children = {								
							cantidadEntry,
							coloresEntry,
							deleteButton
						}
					}
				);

				setPedidoListeners ();
			};

			AddCarritoButton.Clicked += (sender, e) => {
				
			}; 

			setPedidoListeners ();
		}

		void setPedidoListeners ()
		{
			for (int i = 0; i < DeleteRowPedidosButtons.Count; i++) {
				var btn = DeleteRowPedidosButtons [i];
				btn.Clicked += (sender, e) => {			
					PedidoStack.Children.RemoveAt (i);
				};
			}
		}

		Entry getCantidadEntry ()
		{
			return new Entry {
				Placeholder = "#",
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				Keyboard = Keyboard.Numeric,
				WidthRequest = 90,
				HeightRequest = 40,
			};
		}

		Entry getColoresEntry ()
		{
			return new Entry {
				Placeholder = "Colores",
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		Button getDeleteButton ()
		{
			return new Button {
				Text = "Borrar",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Button)),
				TextColor = Color.Red,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}
	}
}

