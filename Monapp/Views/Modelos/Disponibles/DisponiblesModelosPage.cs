﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Monapp
{
	public class DisponiblesModelosPage : ContentPage
	{
		List<ModeloModel> Modelos = new List<ModeloModel>();

		Grid Grid;

		public DisponiblesModelosPage ()
		{
			Title = "Disponibles";	

			//setIndicator ();

			BackgroundColor = ColorResources.Background;

			//Device.BeginInvokeOnMainThread (() => {				
				getModelos ();
				createLayouts ();
				setLayouts ();

				Content = new ScrollView {
					Content = Grid,
				};
			//});
		}

		void setIndicator ()
		{
			var indicator = new ActivityIndicator {
				IsRunning = true,
				Color = ColorResources.NavBarText,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			Content = indicator;
		}

		void getModelos ()
		{
			Modelos = App.WSService.GetModelosAvailables ();
		}

		void createLayouts ()
		{
			Grid = new Grid {
				Padding = new Thickness (10),
				ColumnSpacing = 10,
				ColumnDefinitions = {
					new ColumnDefinition {},
					new ColumnDefinition {}
				},
				RowSpacing = 10,
				VerticalOptions = LayoutOptions.Start,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setLayouts ()
		{			
			if (Modelos.Count > 0) {
				int row = 0;
				int column = 0;
				foreach (var mod in Modelos) {
					Grid.Children.Add (new ModelosTemplate (mod), column, row);
					column++;
					if (column > 1) {
						row++;
						column = 0;
					}
				}
			}
		}
	}
}

