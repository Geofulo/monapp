﻿using System;
using Xamarin.Forms;

namespace Monapp
{
	public class MainPage : MasterDetailPage
	{
		MainMasterPage MasterPage;
		Page DetailPage;

		public MainPage ()
		{
			Title = "Monapp";

			createPages ();
			setListeners ();

			Master = MasterPage;
			Detail = new NavigationPage (DetailPage) {
				BarBackgroundColor = ColorResources.NavBar,
				BarTextColor = ColorResources.NavBarText,
			};
		}

		void createPages ()
		{
			MasterPage = new MainMasterPage ();
			DetailPage = new ModelosPage ();
		}

		void setListeners ()
		{
			MasterPage.MenuListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;				
				var item = e.SelectedItem as string;

				openNavigationPage (item);

				MasterPage.MenuListView.SelectedItem = null;
			};

		}

		void openNavigationPage (string item)
		{
			IsPresented = false;

			if (item.Equals ("Modelos")) {				
				Detail = new NavigationPage (new ModelosPage()) {
					BarBackgroundColor = ColorResources.NavBar,
					BarTextColor = ColorResources.NavBarText,
				};
			}
			if (item.Equals ("Carrito")) {				
				Detail = new NavigationPage (new CarritoPage()) {
					BarBackgroundColor = ColorResources.NavBar,
					BarTextColor = ColorResources.NavBarText,
				};
			}
			if (item.Equals ("Pedidos")) {				
				Detail = new NavigationPage (new PedidosPage()) {
					BarBackgroundColor = ColorResources.NavBar,
					BarTextColor = ColorResources.NavBarText,
				};
			}
			if (item.Equals ("Chat")) {				
				Detail = new NavigationPage (new ChatPage()) {
					BarBackgroundColor = ColorResources.NavBar,
					BarTextColor = ColorResources.NavBarText,
				};
			}
		}
	}
}

