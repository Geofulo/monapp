﻿using System;
using Xamarin.Forms;

namespace Monapp
{
	public class MainMasterPage : ContentPage
	{
		string[] MenuItems = new string[] {
			"Modelos",
			"Carrito",
			"Pedidos",
			"Chat"
		};

		public ListView MenuListView;

		StackLayout Stack;

		public MainMasterPage ()
		{
			Title = "Menú";

			createElements ();
			setLayouts ();

			BackgroundColor = ColorResources.Background;

			Content = new ContentViewCustom {
				IsGradient = true,
				GradientStartColor = ColorResources.BackgroundMenu,
				GradientEndColor = ColorResources.BackgroundMenuSecondary,
				WidthRequest = App.ScreenWidth,
				HeightRequest = App.ScreenHeight,
				Content = Stack,
			};
		}

		void createElements ()
		{
			MenuListView = new ListView {
				ItemsSource = MenuItems,
				RowHeight = 80,
				BackgroundColor = Color.Transparent,
				SeparatorColor = ColorResources.NavBar,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				ItemTemplate = new DataTemplate (() => {
					var cell = new TextCell {						
						TextColor = ColorResources.TextColorMenu,
					};
					cell.SetBinding (TextCell.TextProperty, ".");
					return cell;
				}),
			};
		}

		void setLayouts ()
		{
			Stack = new StackLayout {
				Padding = new Thickness (20, 0, 0, 0),
				Children = {
					MenuListView
				}	
			};
		}
	}
}

