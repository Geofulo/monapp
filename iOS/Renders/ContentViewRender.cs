﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Monapp;
using Monapp.iOS;
using UIKit;
using CoreGraphics;
using CoreAnimation;

[assembly: ExportRenderer(typeof(ContentViewCustom), typeof(ContentViewRender))]

namespace Monapp.iOS
{
	public class ContentViewRender : ViewRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged (e);

			var obj = (ContentViewCustom) this.Element;
			if (Element == null)
				return;

			this.BackgroundColor = obj.BackgroundColor.ToUIColor ();

			if (obj.IsDarkBlur) {
				var blurView = new UIVisualEffectView {
					Frame = new CGRect (0, 0, obj.WidthRequest, obj.HeightRequest),
					//AutosizesSubviews = true,
					Effect = UIBlurEffect.FromStyle (UIBlurEffectStyle.Dark),
				};
				var subviews = this.Subviews;
				this.Subviews.Initialize ();
				this.AddSubview (blurView);
				this.AddSubviews (subviews);
			}
			if (obj.IsLightBlur) {
				var blurView = new UIVisualEffectView {
					Frame = new CGRect (0, 0, obj.WidthRequest, obj.HeightRequest),
					//AutosizesSubviews = true,
					Effect = UIBlurEffect.FromStyle (UIBlurEffectStyle.Light),
				};
				var subviews = this.Subviews;
				this.Subviews.Initialize ();
				this.AddSubview (blurView);
				this.AddSubviews (subviews);
			}

			if (obj.Radius > 0) {
				this.Layer.MasksToBounds = true;
				this.Layer.CornerRadius = obj.Radius;
				this.Layer.BorderWidth = 0;
			}

			if (obj.IsGradient) {
				var gradientLayer = new CAGradientLayer ();
				gradientLayer.Frame = new CGRect (0, 0, obj.WidthRequest, obj.HeightRequest);
				gradientLayer.Colors = new CGColor[] { obj.GradientStartColor.ToCGColor (), obj.GradientEndColor.ToCGColor () };
				this.Layer.InsertSublayer (gradientLayer, 0);
			}

			if (obj.IsShadow) {
				this.Layer.ShadowColor = UIColor.Black.CGColor;
				//this.Layer.ShadowOpacity = 0.2f;
				//this.Layer.BackgroundColor = obj.BackgroundColor.ToCGColor();
			}

		}
	}
}

